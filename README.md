# vue-electron-app

## Link to trello board: https://trello.com/b/Xd4JkOKi/projet-js

## Précisions sur l'utilisation des types au sein du Projet

Pour ce projet, nous avons eu recours à l'utilisation de la ligne suivante dans les différents modules du store:
```
eslint-disable @typescript-eslint/no-explicit-any
```
Ce choix d'implémentation s'explique par le fait que Vue n'est pas conçu pour fonctionner avec TypeScript nativement, ce 
qui induit que les types que nous utilisons ont été écrits pas une communauté différente de celle qui développe Vue. 
Certains types que l'on retrouve dans le store sont trop complexes et ont donc été typés par ces mêmes développeurs avec des
`any`. Pour pouvoir créer des fonctions qui travaillent sur ces types, nous avons donc dû également typer, en partie, avec des `any`. De ce fait, nous avons désactivé la règle `no-explicit-any` dans le store, afin de limiter le nombre de warnings "inutiles".

## Système de communication avec caméra et microphone

Nous avons également utilisé les URLs suivantes pour débugger le WebRTC et les connexions P2P dans les navigateurs:
```
chrome://webrtc-internals (Chrome)
about:webrtc (Firefox) 
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
