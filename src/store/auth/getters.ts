import { StateAuth } from "./../../utils/types";

const getters = {
    token: (state: StateAuth) => state.token
};

export default getters;
