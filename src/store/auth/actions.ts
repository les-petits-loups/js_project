/* eslint-disable @typescript-eslint/no-explicit-any */
import Vue from "vue";
import VueCookies from "vue-cookies";
import { Login, APILoginResponse, APIVerifyTokenResponse } from "@/utils/types";
import endpoints from "@/config/endpoints";
import { ActionContext } from "vuex";

Vue.use(VueCookies);

export default {
    async isLoginInfoCorrect(
        _: ActionContext<any, unknown>,
        { email, password }: Login
    ) {
        let isLoginInfoCorrect = false;

        await fetch(endpoints.login, {
            method: "POST",
            body: JSON.stringify({ email, password }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then((response: Response) => response.json())
            .then((response: APILoginResponse) => {
                if (response.error === "") {
                    isLoginInfoCorrect = true;
                    Vue.$cookies.set("token", response.token);
                }
            })
            .catch(error => {
                console.error(error);
            });

        return isLoginInfoCorrect;
    },
    async isTokenValid() {
        let isTokenValid = false;
        const token = Vue.$cookies.get("token")
            ? Vue.$cookies.get("token")
            : "none";

        await fetch(endpoints.token, {
            method: "POST",
            body: JSON.stringify({ token }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Authorization: token
            }
        })
            .then((response: Response) => response.json())
            .then((response: APIVerifyTokenResponse) => {
                if (response.error === "") {
                    isTokenValid = true;
                    localStorage.setItem("id", String(response.id));
                }
            })
            .catch(error => {
                console.error(error);
            });

        return isTokenValid;
    }
};
