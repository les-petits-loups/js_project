import state from "./state";
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";

const AuthStore = {
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions
};

export default AuthStore;
