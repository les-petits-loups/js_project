import { StateAuth } from "./../../utils/types";

export default {
    changeToken(state: StateAuth, token: string) {
        state.token = token;
    }
};
