/* eslint-disable @typescript-eslint/no-explicit-any */
import Vue from "vue";
import VueCookies from "vue-cookies";
import { Theme, APIThemeResponse, APIThemesResponse } from "@/utils/types";
import endpoints from "@/config/endpoints";
import { ActionContext } from "vuex";

Vue.use(VueCookies);

export default {
    async getTheme() {
        let theme: Theme | null = null;

        await fetch(endpoints.theme, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Authorization: Vue.$cookies.get("token")
            }
        })
            .then((response: Response) => response.json())
            .then((data: APIThemeResponse) => {
                if (data.error === "") {
                    theme = data.theme;
                }
            })
            .catch(error => console.error(error));

        return theme;
    },
    async getThemes(
        _: ActionContext<any, unknown>,
        { page }: { page: number }
    ) {
        const result: { themes: Theme[]; pages: number } = {
            themes: [],
            pages: 0
        };

        await fetch(endpoints.themes + `?page=${page}`, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Authorization: Vue.$cookies.get("token")
            }
        })
            .then((response: Response) => response.json())
            .then((data: APIThemesResponse) => {
                if (data.error === "") {
                    result.themes = data.themes;
                    result.pages = data.pages;
                }
            })
            .catch(error => {
                console.error(error);
            });

        return result;
    },
    async getFilteredThemes(
        _: ActionContext<any, unknown>,
        { page, search }: { page: number; search: string }
    ) {
        const result: { themes: Theme[]; pages: number } = {
            themes: [],
            pages: 0
        };

        await fetch(
            endpoints.filteredThemes + `?page=${page}&search=${search}`,
            {
                method: "GET",
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    Authorization: Vue.$cookies.get("token")
                }
            }
        )
            .then((response: Response) => response.json())
            .then((data: APIThemesResponse) => {
                if (data.error === "") {
                    result.themes = data.themes;
                    result.pages = data.pages;
                }
            })
            .catch(error => {
                console.error(error);
            });

        return result;
    }
};
