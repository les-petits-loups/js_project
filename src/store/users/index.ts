import state from "./state";
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";

const UsersStore = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
};

export default UsersStore;
