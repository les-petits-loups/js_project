import { StateUser } from "../../utils/types";

const state: StateUser = {
  wsConnection: null
};

export default state;
