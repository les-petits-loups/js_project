/* eslint-disable @typescript-eslint/no-explicit-any */
import Vue from "vue";
import VueCookies from "vue-cookies";
import {
    User,
    APIUserResponse,
    APIRankingResponse,
    APIUsersResponse
} from "@/utils/types";
import endpoints from "@/config/endpoints";
import { ActionContext } from "vuex";

Vue.use(VueCookies);

export default {
    registerNewUser(
        _: ActionContext<any, unknown>,
        { firstName, lastName, email, password }: User
    ) {
        fetch(endpoints.user, {
            method: "POST",
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
            .then((response: APIUserResponse) => {
                console.log(response.error);
            })
            .catch(error => console.error(error));
    },
    async getUserById(_: ActionContext<any, unknown>, { id }: { id: number }) {
        let user: User | null = null;

        await fetch(endpoints.user + `/${id}`, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Authorization: Vue.$cookies.get("token")
            }
        })
            .then((response: Response) => response.json())
            .then((response: APIUserResponse) => {
                if (response.error === "") {
                    user = response.user;
                }
            })
            .catch(error => {
                console.error(error);
            });

        return user;
    },
    async getRanking(_: ActionContext<any, unknown>, { id }: { id: number }) {
        let ranking;

        await fetch(endpoints.ranking + `/${id}`, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Authorization: Vue.$cookies.get("token")
            }
        })
            .then((response: Response) => response.json())
            .then((response: APIRankingResponse) => {
                if (response.error === "") {
                    ranking = response;
                }
            })
            .catch(error => {
                console.error(error);
            });

        return ranking;
    },
    async isEmailTaken(_: ActionContext<any, unknown>, email: string) {
        let emailTaken = false;

        await fetch(endpoints.user + `?page=1&search=${email}&register=1`, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
            .then((response: APIUsersResponse) => {
                if (response.error === "") {
                    response.users.forEach((user: User) => {
                        if (user.email.toLowerCase() === email.toLowerCase()) {
                            emailTaken = true;
                        }
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });

        return emailTaken;
    },
    createConnection({ commit }: { commit: Function }) {
        commit("createConnection");
    },
    closeConnection({ commit }: { commit: Function }) {
        commit("closeConnection");
    }
};
