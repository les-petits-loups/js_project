export default {
    createConnection(state: any) {
        state.wsConnection = new WebSocket("ws://localhost:8080");
    },
    closeConnection(state: any) {
        if (state.wsConnection !== null) {
            state.wsConnection.close();
            state.wsConnection = null;
        }
    }
};
