import Vue from "vue";
import Vuex from "vuex";
import AuthStore from "./auth/index";
import UsersStore from "./users/index";
import ThemesStore from "./themes/index";
import QuizzsStore from "./quizzs/index";
import { Store } from "../utils/types";

Vue.use(Vuex);

const store: Store | any = new Vuex.Store({
    modules: {
        auth: AuthStore,
        users: UsersStore,
        themes: ThemesStore,
        quizzs: QuizzsStore
    }
});

export default store;
