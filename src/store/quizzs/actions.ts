/* eslint-disable @typescript-eslint/no-explicit-any */
import Vue from "vue";
import VueCookies from "vue-cookies";
import {
    Question,
    Quizz,
    PossibleAnswer,
    APIQuestionResponse,
    APIPossibleAnswersQuestionResponse,
    APIPossibleAnswerResponse,
    APIQuizzResponse,
    APIQuestionsResponse
} from "@/utils/types";
import endpoints from "@/config/endpoints";
import { ActionContext } from "vuex";

Vue.use(VueCookies);

export default {
    async getQuestionById(
        _: ActionContext<any, unknown>,
        { id }: { id: number }
    ) {
        let question: Question | null = null;

        await fetch(endpoints.question + "/" + id, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Authorization: Vue.$cookies.get("token")
            }
        })
            .then((response: Response) => response.json())
            .then((response: APIQuestionResponse) => {
                if (response.error === "") {
                    question = response.question;
                }
            })
            .catch(error => {
                console.error(error);
            });

        return question;
    },
    async getAnswerById(
        _: ActionContext<any, unknown>,
        { id }: { id: number }
    ) {
        let possibleAnswer: PossibleAnswer | null = null;

        await fetch(endpoints.possibleAnswer + "/" + id, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Authorization: Vue.$cookies.get("token")
            }
        })
            .then((response: Response) => response.json())
            .then((response: APIPossibleAnswerResponse) => {
                if (response.error === "") {
                    possibleAnswer = response.possibleAnswer;
                }
            })
            .catch(error => {
                console.error(error);
            });

        return possibleAnswer;
    },
    registerQuizzSolo(
        _: ActionContext<any, unknown>,
        { mode, user1, questions }: Quizz
    ) {
        fetch(endpoints.quizz, {
            method: "POST",
            body: JSON.stringify({
                mode,
                user1,
                startAt: new Date(),
                questions
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Authorization: Vue.$cookies.get("token")
            }
        })
            .then(response => response.json())
            .then((response: APIQuizzResponse) => {
                console.log(response.error);
            })
            .catch(error => console.error(error));
    },
    async getFourRandomQuestionsByTheme(
        _: ActionContext<any, unknown>,
        { id }: { id: number }
    ) {
        let questions: Question[] | null = null;

        await fetch(endpoints.question + `?theme=${id}&random=1`, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Authorization: Vue.$cookies.get("token")
            }
        })
            .then((response: Response) => response.json())
            .then((response: APIQuestionsResponse) => {
                if (response.error === "") {
                    questions = response.questions;
                }
            })
            .catch(error => {
                console.error(error);
            });

        return questions;
    },
    async getQuestionPossibleAnswers(
        _: ActionContext<any, unknown>,
        { id }: { id: number }
    ) {
        let possibleAnswers: PossibleAnswer[] | null = null;

        await fetch(endpoints.possibleAnswersQuestion + "/" + id, {
            method: "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Authorization: Vue.$cookies.get("token")
            }
        })
            .then((response: Response) => response.json())
            .then((response: APIPossibleAnswersQuestionResponse) => {
                if (response.error === "") {
                    possibleAnswers = response.possibleAnswers;
                }
            })
            .catch(error => {
                console.error(error);
            });

        return possibleAnswers;
    }
};
