export type LoginToken = {
    email: string;
    connected: boolean;
};

export type Login = {
    email: string;
    password: string;
};

export type User = {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    points: number;
    registerAt: Date;
};

export type Theme = {
    id: number;
    name: string;
};

export type Question = {
    id: number;
    label: string;
    theme: number;
};

export type Quizz = {
    id?: number | null;
    mode: number;
    user1: number | User;
    user2?: number | User | null;
    questions: number[] | Question[];
    winner?: number | User | null;
    startAt: Date;
};

export type PossibleAnswer = {
    id: number;
    question: Question | null;
    label: string;
    isRight: boolean;
};

export type APIResponse = {
    error: string;
};

export type APIQuizzResponse = {
    quizz: Quizz;
    error: string;
};

export type APILoginResponse = {
    token: string;
    id: number;
    error: string;
};

export type APIUserResponse = {
    user: User;
    error: string;
};

export type APIVerifyTokenResponse = {
    id: number;
    error: string;
};

export type APIUsersResponse = {
    users: User[];
    error: string;
    pages?: number;
};

export type APIThemeResponse = {
    theme: Theme;
    error: string;
};

export type APIThemesResponse = {
    pages: number;
    themes: Theme[];
    error: string;
};

export type APIRankingResponse = {
    ranking: number;
    numberOfUsers: number;
    error: string;
};

export type APIQuestionResponse = {
    question: Question;
    error: string;
};

export type APIQuestionsResponse = {
    questions: Question[];
    error: string;
};

export type APIPossibleAnswersQuestionResponse = {
    possibleAnswers: PossibleAnswer[];
    error: string;
};

export type APIPossibleAnswerResponse = {
    possibleAnswer: PossibleAnswer;
    error: string;
};

export type Rules = {
    email?: Function[];
    password?: Function[];
    passwordConfirm?: Function[];
    firstName?: Function[];
    lastName?: Function[];
};

export type StateUser = {
    wsConnection: any;
};

export type StateAuth = {
    token: string;
};

export type Store = {
    state: {
        users: StateUser;
        auth: any;
        quizzs: any;
        themes: any;
    };
    dispatch: Function;
};

export type UserToken = {
    id: number;
};
