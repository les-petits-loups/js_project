import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Main from "@/Main.vue";
import Login from "@/Login.vue";
import StartQuizzDuo from "@/views/StartQuizzDuo.vue";
import TakeQuizzDuo from "@/views/TakeQuizzDuo.vue";
import TakeQuizzSolo from "@/views/TakeQuizzSolo.vue";
import StartQuizzSolo from "@/views/StartQuizzSolo.vue";
import UserInfo from "@/views/UserInfo.vue";
import Register from "@/Register.vue";
import PageNotFound from "@/PageNotFound.vue";
import store from "../store";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
    {
        path: "/",
        meta: { requiresAuth: true },
        component: Main,
        children: [
            {
                path: "/start-quizz-solo",
                name: "start-quizz-solo",
                component: StartQuizzSolo
            },
            {
                path: "/take-quizz-solo",
                name: "take-quizz-solo",
                component: TakeQuizzSolo
            },
            {
                path: "/start-quizz-duo",
                name: "start-quizz-duo",
                component: StartQuizzDuo
            },
            {
                path: "/take-quizz-duo",
                name: "take-quizz-duo",
                component: TakeQuizzDuo
            },
            {
                path: "/user-info",
                name: "user-info",
                component: UserInfo
            }
        ]
    },
    {
        path: "/login",
        component: Login
    },
    {
        path: "/register",
        component: Register
    },
    {
        path: "*",
        component: PageNotFound
    }
];

const router = new VueRouter({
    mode: process.env.IS_ELECTRON ? "hash" : "history",
    base: process.env.BASE_URL,
    routes
});

router.beforeEach(async (to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        const isTokenValid = await store.dispatch("isTokenValid");
        if (!isTokenValid) {
            next({
                path: "/login",
                query: { redirect: to.fullPath }
            });
        } else {
            next();
        }
    } else {
        next(); // make sure to always call next()!
    }
});

export default router;
