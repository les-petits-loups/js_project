import config from "./config";

const domain = config.domain;

const endpoints = {
    user: `http://${domain}/api/user`,
    ranking: `http://${domain}/api/ranking`,
    token: `http://${domain}/api/user/token`,
    login: `http://${domain}/api/user/login`,
    question: `http://${domain}/api/question`,
    possibleAnswersQuestion: `http://${domain}/api/answer/question`,
    possibleAnswer: `http://${domain}/api/answer`,
    quizz: `http://${domain}/api/quizz`,
    theme: `http://${domain}/api/theme`,
    themes: `http://${domain}/api/theme`,
    filteredThemes: `http://${domain}/api/theme`
};
export default endpoints;
